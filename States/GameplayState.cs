using Game.Entities;

using static Raylib_cs.KeyboardKey;
using static Raylib_cs.Raylib;
using static Raylib_cs.Color;
using static Game.GlobalData;

namespace Game.States
{
    public class GameplayState : State
    {
        private int PointsPlayer1 { get; set; } = 0;
        private int PointsPlayer2 { get; set; } = 0;

        private int scoreboardTextWidth;
        private const int PaddleWidth = 20;
        private const int PaddleHeight = 100;
        private const int BallRadius = 10;

        private readonly Paddle paddleLeft, paddleRight;
        private Ball ball;

        public GameplayState()
        {
            scoreboardTextWidth = MeasureText(GetScoreboardText(), FontSize);
            paddleLeft = new(0, WindowHeight / 2 - (PaddleHeight / 2), PaddleWidth, PaddleHeight, Sides.Left);
            paddleRight = new(WindowWidth - PaddleWidth, WindowHeight / 2 - (PaddleHeight / 2), PaddleWidth, PaddleHeight, Sides.Right);
            ball = new(BallRadius);
            ball.Reset();
        }

        public override void Update()
        {
            HandleInput();
            HandleCollision();
            HandlePoints();

            ball.Move();
        }

        public override void Draw()
        {
            BeginDrawing();
            {
                ClearBackground(WHITE);

                DrawCircle(ball.X, ball.Y, ball.Radius, RED);
                DrawRectangle(paddleLeft.X, paddleLeft.Y, paddleLeft.Width, paddleLeft.Height, BLACK);
                DrawRectangle(paddleRight.X, paddleRight.Y, paddleRight.Width, paddleRight.Height, BLACK);
                DrawText(GetScoreboardText(), (WindowWidth / 2) - (scoreboardTextWidth / 2), 20, 20, BLACK);
            }
            EndDrawing();
        }

        private void HandleCollision()
        {
            ball.HandleCollisionWithPaddle(paddleLeft);
            ball.HandleCollisionWithPaddle(paddleRight);

            ball.HandleAreaCollision();
            paddleLeft.HandleCollision();
            paddleRight.HandleCollision();
        }

        private void HandleInput()
        {
            if (IsKeyDown(KEY_W))
                paddleLeft.Y -= Paddle.Speed;
            if (IsKeyDown(KEY_S))
                paddleLeft.Y += Paddle.Speed;

            if (IsKeyDown(KEY_UP))
                paddleRight.Y -= Paddle.Speed;
            if (IsKeyDown(KEY_DOWN))
                paddleRight.Y += Paddle.Speed;

            if (IsKeyPressed(KEY_SPACE))
                EnterState(new PauseMenuState());

            else if (IsKeyPressed(KEY_BACKSPACE))
                ExitState();
        }

        private void HandlePoints()
        {
            if (ball.CollidesWithLeftSide())
                PointsPlayer2++;

            if (ball.CollidesWithRightSide())
                PointsPlayer1++;
        }

        private string GetScoreboardText() => $"{PointsPlayer1} : {PointsPlayer2}";
    }
}
