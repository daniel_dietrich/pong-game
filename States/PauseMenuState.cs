using static Raylib_cs.Raylib;
using static Raylib_cs.KeyboardKey;
using static Raylib_cs.Color;
using static Game.GlobalData;

namespace Game.States
{
    public class PauseMenuState : State
    {
        private const string TitleText = "PAUSE SCREEN";
        private readonly int titleTextWidth = MeasureText(TitleText, FontSize);
        private readonly int titleTextX;
        private readonly int titleTextY;

        const string DescriptionText = "PRESS [BACKSPACE] TO CONTINUE";
        private readonly int descriptionTextWidth = MeasureText(DescriptionText, FontSize);
        private readonly int descriptionTextX;
        private readonly int descriptionTextY;

        public PauseMenuState()
        {
            titleTextX = (WindowWidth / 2) - (titleTextWidth / 2);
            titleTextY = (WindowHeight / 2) - FontSize;
            
            descriptionTextX = (WindowWidth / 2) - descriptionTextWidth / 2;
            descriptionTextY = (WindowHeight / 2) + FontSize;
        }

        public override void Update()
        {
            if (IsKeyPressed(KEY_BACKSPACE)) { ExitState(); }
        }

        public override void Draw()
        {
            BeginDrawing();
            {
                ClearBackground(DARKBLUE);

                DrawText(TitleText, titleTextX, titleTextY, FontSize, WHITE);
                DrawText(DescriptionText, descriptionTextX, descriptionTextY, FontSize, WHITE);
            }
            EndDrawing();
        }
    }
}
