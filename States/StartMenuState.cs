using static Raylib_cs.Raylib;
using static Raylib_cs.KeyboardKey;
using static Raylib_cs.Color;
using static Game.GlobalData;

namespace Game.States
{
    public class StartMenuState : State
    {
        private const string TitleText = "START SCREEN";
        private readonly int titleTextWidth = MeasureText(TitleText, FontSize);
        private readonly int titleTextX;
        private readonly int titleTextY;

        private const string DescriptionText = "PRESS [SPACE] TO START";
        private readonly int descriptionTextWidth = MeasureText(DescriptionText, FontSize);
        private readonly int descriptionTextX;
        private readonly int descriptionTextY;

        public StartMenuState()
        {
            titleTextX = (WindowWidth / 2) - titleTextWidth / 2;
            titleTextY = (WindowHeight / 2) - FontSize;
            
            descriptionTextX = (WindowWidth / 2) - descriptionTextWidth / 2;
            descriptionTextY = (WindowHeight / 2) + FontSize;
        }

        public override void Update()
        {
            if (IsKeyPressed(KEY_SPACE)) { EnterState(new GameplayState()); }
        }

        public override void Draw()
        {
            BeginDrawing();
            {
                ClearBackground(DARKGREEN);

                DrawText(TitleText, titleTextX, titleTextY, FontSize, WHITE);
                DrawText(DescriptionText, descriptionTextX, descriptionTextY, FontSize, WHITE);
            }
            EndDrawing();
        }
    }
}
