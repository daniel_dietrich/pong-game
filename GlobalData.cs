using System.IO;

namespace Game
{
    public static class GlobalData
    {
        public const string WindowTitle = "Game";
        public const int WindowWidth = 640;
        public const int WindowHeight = 480;
        public const int FontSize = 20;
        public const int FPS = 60;
        public static readonly string SoundsPath = Path.Combine(Directory.GetCurrentDirectory(), "Sounds");
    }
}
